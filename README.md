# lxcscripts

This repository contains various scripts and files related to LXC.

## list-lxc

Simply calls "lxc-ls" with some commonly used parameters.

## restart-lxc

Restarts a container by calling lxc-stop and lxc-start and waits for the file "/run/dhclient.eth0.pid" to be created.

## create_container_*

Scripts to automatically create containers with (almost) ready-to-use software.

### fdroidserver

Container featuring fdroidserver and nginx. nginx is configured to use self-signed certificates for SSL. In production use, those should be replaced by certificates accepted by clients.

### mediawiki

Creates a container with MediaWiki based on Debian 9 (stretch). Should not be used for new deployments!

### ubu1804_mw1.31.3

Creates a container with MediaWiki 1.31.3 based on Ubuntu 18.04. The MediaWiki installation archive (mediawiki-1.31.3.tar.gz) will be automatically downloaded to /root/Downloads if necessary.

## vmnet

Creates a bridge with the name "vmnet" that can be used if lxcbr0 cannot be used or has issues.

## lxc-net.in

Provides my version of the lxc-net initialisation script that handles nft instead of iptables. Please note: Initially it was my attempt to quickly provide a version that runs on Debian 10 (buster). In January 2022 I had the need to get this running in Debian 11 (bullseye). I did not pay much attention to good code quality in this version. Also, I didn't bother if it can be used on other systems. If my time allows further improvements, I will apply them, but please don't expect them.

My starting point for development was https://github.com/lxc/lxc/blob/21dbd2eef238511f432c79c2e151b304ec03b43c/config/init/common/lxc-net.in which unfortunately doesn't work (mainly because it uses `nft delete` in a way that doesn't work yet). By 23 January 2022, there's a new version at https://github.com/lxc/lxc/blob/master/config/init/common/lxc-net.in that includes support for nft, but I have to test it first.

### Installation

I put this file in /usr/local/bin. /usr/lib/systemd/system/lxc-net.service calls `/usr/lib/x86_64-linux-gnu/lxc/lxc-net start` and `/usr/lib/x86_64-linux-gnu/lxc/lxc-net stop` so I renamed /usr/lib/x86_64-linux-gnu/lxc/lxc-net to /usr/lib/x86_64-linux-gnu/lxc/lxc-net.old and created a symlink from /usr/lib/x86_64-linux-gnu/lxc/lxc-net to /usr/local/bin/lxc-net.in.

### Helpful documentation

* https://wiki.nftables.org/wiki-nftables/index.php/Moving_from_iptables_to_nftables
* https://wiki.nftables.org/wiki-nftables/index.php/Quick_reference-nftables_in_10_minutes
* https://wiki.nftables.org/wiki-nftables/index.php/Simple_rule_management
* https://wiki.gentoo.org/wiki/Nftables/Examples
* https://docs.snowme34.com/en/latest/reference/devops/debian-firewall-nftables-and-iptables.html

### Improvements

* Refactor iptables_start_rules (call the nft-part in a loop)
* Refactor nftables_stop_rules (call the if/nft-part in a loop)
* Currently it is assumed that rules should be added to "inet filter", this should be configurable in /etc/default/lxc
